/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

/**
 *
 * @author decline
 */
public class JugadorEstandar extends JugadorAbstracte {

    private String nom;

    /**
     * Constructor de JugadorEstandar
     * @param strat Estratègia de jugador
     * @param nom Nom del jugador
     */
    public JugadorEstandar(IEstrategia strat, String nom) {
        super(strat, nom);
    }
    
    /**
     * Executa l'ordre a l'etratègia per a que retorni un resultat
     * @return El valor de la tirada
     */
    @Override
    public int ferTirada(){
        return estrategia.calcularTirada();
    }
}

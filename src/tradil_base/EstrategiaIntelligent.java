/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import java.util.*;

/**
 *
 * @author decline
 */
public class EstrategiaIntelligent implements IEstrategia, Observer {

    private List<Integer> tirades;

    /**
     * Constructor
     */
    public EstrategiaIntelligent() {
        tirades = new ArrayList();
    }

    /**
     * Actualitza la llista amb la notifiació de l'Observable
     * @param partida Objecte partida
     * @param ronda Ronda que s'ha executat
     */
    @Override
    public void update(Observable partida, Object ronda) {
        RondaAbstracta ronda2 = (RondaAbstracta) ronda;
        int acum = 0;
        for (int res : ronda2.getResultat()) {
            acum += res;
        }
        tirades.add(acum / ronda2.getResultat().size());
    }

    /**
     * Calcula la tirada segons les rondes anteriors
     * @return el nombre màxim de les tirades anteriors.
     */
    @Override
    public int calcularTirada() {
        int total = Integer.MIN_VALUE;
        if (tirades.size() == 0) {
            Random r = new Random();
            return r.nextInt(52) + 2; // En cas de dubte 50% de possibilitats de guanyar la ronda :)
        } else {
            for (int t : tirades) {
                total = Math.max(t, total);
            }
            return total;
        }
    }
}

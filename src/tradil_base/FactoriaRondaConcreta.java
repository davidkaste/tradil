/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

/**
 *
 * @author decline
 */
public class FactoriaRondaConcreta implements IFactoriaRondes {

    /**
     * Crea una nova ronda
     * @return Objecte Ronda
     */
    public RondaAbstracta createRonda() {
        return new RondaConcreta();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author decline
 */
public class ObservadorAcumulat implements Observer {

    private int acum_jugador1;
    private int acum_jugador2;

    public ObservadorAcumulat() {
        acum_jugador1 = 0;
        acum_jugador2 = 0;
    }
    
    /**
     * Actualitza i mostra per pantalla el resultat de la ronda. (Acumulat)
     * @param partida Objecte observable
     * @param ronda La resposta del observable
     */
    @Override
    public void update(Observable partida, Object ronda) {
        RondaAbstracta ronda2 = (RondaAbstracta) ronda;
        System.out.println("Estat ronda: (Acumulat)");
        acum_jugador1 += ronda2.getResultat().get(0);
        acum_jugador2 += ronda2.getResultat().get(1);
        Partida part = (Partida) partida;
        System.out.println(part.getJugadors().get(0).getNom()+": " + acum_jugador1);
        System.out.println(part.getJugadors().get(1).getNom()+": " + acum_jugador2);
    }
}

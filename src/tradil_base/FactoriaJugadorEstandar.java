/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

/**
 *
 * @author decline
 */
public class FactoriaJugadorEstandar implements IFactoriaJugadors {
    /**
     * Crea un nou jugador
     * @param strat Estratègia del jugador
     * @param nom Nom del jugador
     * @return Objecte Jugador
     */
    public JugadorAbstracte createJugador(IEstrategia strat, String nom){
        return new JugadorEstandar(strat, nom);
    }
}

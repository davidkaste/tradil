/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author David Castellà <decline.corp@gmail.com>
 */
public class Partida extends Observable {

    private List<RondaAbstracta> rondes;
    private List<JugadorAbstracte> jugadors;
    
    /**
     * constructor de la partida
     * @param builder Builder de la partida
     */
    public Partida(ConfiguradorPartida builder){
        rondes = builder.getRondes();
        jugadors = builder.getJugadors();
        for(java.util.Observer obs: builder.getObservadors()){
            this.addObserver(obs);
        }
    }

    /**
     * Comença la partida
     */
    public void executarPartida() {
        List<Integer> resultat;
        for (RondaAbstracta r : rondes) {
            r.executarRonda(jugadors);
            this.setChanged();
            this.notifyObservers(r);
        }
    }

    /**
     * Retorna la llista de jugadors que formen la partida
     * @return Atribut jugadors
     */
    public List<JugadorAbstracte> getJugadors() {
        return jugadors;
    }

    /**
     * Retorna la llista de rondes que formen la partida
     * @return Atribut rondes
     */
    public List<RondaAbstracta> getRondes() {
        return rondes;
    }
}

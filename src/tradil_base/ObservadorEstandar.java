/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author decline
 */
public class ObservadorEstandar implements Observer {

    /**
     * Actualitza i mostra per pantalla el resultat de la ronda.
     * @param partida Objecte observable
     * @param ronda La resposta del observable
     */
    @Override
    public void update(Observable partida, Object ronda) {
        RondaAbstracta ronda2 = (RondaAbstracta) ronda;
        Partida part = (Partida) partida;
        System.out.println("Estat ronda:");
        int index_jugador = 0;
        for (int res : ronda2.getResultat()) {
            System.out.println(part.getJugadors().get(index_jugador).getNom() + ": " + res);
            index_jugador++;
        }
    }
}

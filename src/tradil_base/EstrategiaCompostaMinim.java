/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import java.util.Iterator;

/**
 *
 * @author decline
 */
public class EstrategiaCompostaMinim extends EstrategiaComposta {

    /**
     * Calcula el nombre mínim de les tirades de les estratègies que composen la llista.
     * @return Nombre mínim
     */
    @Override
    public int calcularTirada() {
        int sumatori_tirades = Integer.MAX_VALUE;
        Iterator<IEstrategia> iter = estrategies.iterator();
        while (iter.hasNext()) {
            sumatori_tirades = Math.min(iter.next().calcularTirada(), sumatori_tirades);
        }
        return (sumatori_tirades / estrategies.size());
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

/**
 *
 * @author David Castellà <decline.corp@gmail.com>
 */
public interface IFactoriaRondes {
    /**
     * Declaració del mètode createRonda
     * @return Una instància de ronda
     */
    public RondaAbstracta createRonda();
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author decline
 */
public abstract class RondaAbstracta {

    protected List<Integer> resultat;
    protected boolean executada;

    /**
     * Constructor
     */
    public RondaAbstracta() {
        resultat = new ArrayList();
        executada = false;
    }

    /**
     * Retorna el resultat de la ronda
     * @return atribut resultat
     */
    public List<Integer> getResultat() {
        return resultat;        
    }
    
    /**
     * ens diu si la ronda ha estat executada.
     * @return true o false
     */
    public boolean isExecutada(){
        return executada;
    }
    
    /**
     * Finalitza la ronda actual
     */
    private void finalitzarRonda(){
        executada = true;
    }
    
    /**
     * Executa la ronda.
     * @param llista_jugadors 
     */
    public void ferRonda(List<JugadorAbstracte> llista_jugadors){
        executarRonda(llista_jugadors);
        finalitzarRonda();
    }
    
    /**
     * Declaració del mètode executarRonda
     * @param llista_jugadors jugadors que formen la ronda
     */
    protected abstract void executarRonda(List<JugadorAbstracte> llista_jugadors);
}

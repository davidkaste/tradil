/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import java.util.Random;

/**
 *
 * @author decline
 */
public class EstrategiaAleatoria implements IEstrategia {

    Random generador;

    /**
     * Constructor
     */
    public EstrategiaAleatoria() {
        generador = new Random();
    }

    /*
     * Constructor amb el paràmetre de la llavor
     */
    public EstrategiaAleatoria(int llavor) {
        generador = new Random(llavor);
    }

    /**
     * Calcula un nombre aleatori entre 2 i 100
     * @return nombre aleatori
     */
    public int calcularTirada() {
        return generador.nextInt(99) + 2;
    }
}

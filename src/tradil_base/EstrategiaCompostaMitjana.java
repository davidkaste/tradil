/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import java.util.Iterator;

/**
 *
 * @author decline
 */
public class EstrategiaCompostaMitjana extends EstrategiaComposta {

    /**
     * Calcula el nombre mitjà de les tirades de les estratègies que composen la llista.
     * @return mitjana de les tirades
     */
    @Override
    public int calcularTirada() {
        int sumatori_tirades = 0;
        Iterator<IEstrategia> iter = estrategies.iterator();
        while (iter.hasNext()) {
            sumatori_tirades += iter.next().calcularTirada();
        }
        return (sumatori_tirades / estrategies.size());
    }
}

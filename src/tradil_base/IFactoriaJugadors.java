/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

/**
 *
 * @author decline
 */
public interface IFactoriaJugadors {
    /**
     * Declaració del mètode createJugador
     * @param strat Estratègia del Jugador
     * @param nom Nom del JKugador
     * @return Una instància de jugador
     */
    public JugadorAbstracte createJugador(IEstrategia strat, String nom);
}

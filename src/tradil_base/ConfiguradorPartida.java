/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author decline
 */
public class ConfiguradorPartida {

    private List<RondaAbstracta> rondes;
    private List<JugadorAbstracte> jugadors;
    private List<java.util.Observer> observadors;

    /**
     * Constructor del Configurador de la partida
     */
    public ConfiguradorPartida() {
        rondes = new ArrayList();
        jugadors = new ArrayList();
        observadors = new ArrayList();
    }

    /**
     * Retorna la llista de rondes.
     * @return rondes
     */
    public List<RondaAbstracta> getRondes() {
        return rondes;
    }

    /**
     * Retorna la llista de jugadors
     * @return jugadors
     */
    public List<JugadorAbstracte> getJugadors() {
        return jugadors;
    }
    
    /**
     * Retorna la llista d'observadors
     * @return observadors
     */
    public List<java.util.Observer> getObservadors() {
        return observadors;
    }

    /**
     * Inicialitza la llista de rondes que tindrà la partida
     * @param fact_rondes Factoria de rondes
     * @param num_rondes Número de rondes
     * @return this
     */
    public ConfiguradorPartida setRondes(IFactoriaRondes fact_rondes, int num_rondes) {
        for (int i = 0; i < num_rondes; i++) {
            rondes.add(fact_rondes.createRonda());
        }
        return this;
    }

    /**
     * Afegeix un jugador creat per la factoria amb l'estratègia definida.
     * @param fact_jugador Factoria de jugadors
     * @param strat Estratègia del jugador
     * @param nom Nom del jugador
     * @return this
     */
    public ConfiguradorPartida addJugador(IFactoriaJugadors fact_jugador, IEstrategia strat, String nom) {
        jugadors.add(fact_jugador.createJugador(strat, nom));
        return this;
    }
    
    /**
     * Afegeix un observador a la llista
     * @param obs
     * @return this
     */
    public ConfiguradorPartida addObserver(java.util.Observer obs){
        observadors.add(obs);
        return this;
    }

    /**
     * Construeix una partida a partir de la classe.
     * @return new Partida(this)
     */
    public Partida build() {
        return new Partida(this);
    }
}

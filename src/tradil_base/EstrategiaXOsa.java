/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

/**
 *
 * @author decline
 */
public class EstrategiaXOsa implements IEstrategia {

    private int numero;

    /**
     * Constructor
     * @param valor_defecte Valor que respondrà a cada tirada
     */
    public EstrategiaXOsa(int valor_defecte) {
        numero = valor_defecte;
    }

    /**
     * Calcula la tirada
     * @return Atribut numero
     */
    public int calcularTirada() {
        return numero;
    }
}

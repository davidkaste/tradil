/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

/**
 *
 * @author decline
 */
public abstract class JugadorAbstracte {

    protected IEstrategia estrategia;
    protected String nom;

    /**
     * Constructor de JugadorAbstracte
     * @param strat Estratègia del jugador
     * @param nom Nom del jugador
     */
    public JugadorAbstracte(IEstrategia strat, String nom) {
        estrategia = strat;
        this.nom = nom;
    }
    
    /**
     * Retorna el nom del jugador
     * @return Atribut nom
     */
    public String getNom(){
        return nom;
    }

    /**
     * Declaració del mètode ferTirada
     * @return Retorna un enter amb el valor de la tirada.
     */
    public abstract int ferTirada();
}

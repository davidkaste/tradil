/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

/**
 *
 * @author David Castellà <decline.corp@gmail.com>
 */
public interface IEstrategia {

    /**
     * Deficinió del mètode calcularTirada
     * @return Valor de la tirada
     */
    public int calcularTirada();
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import java.util.List;

/**
 *
 * @author decline
 */
public class RondaConcreta extends RondaAbstracta {

    /**
     * Fa que els jugadors facin les seves tirades.
     * @param llista_jugadors jugadors que formen la ronda.
     */
    @Override
    public void executarRonda(List<JugadorAbstracte> llista_jugadors) {
        int tirada_jugador1 = llista_jugadors.get(0).ferTirada();
        int tirada_jugador2 = llista_jugadors.get(1).ferTirada();
        if (tirada_jugador1 < tirada_jugador2) {
            tirada_jugador2 = tirada_jugador1 - 2;
            tirada_jugador1 += 2;
        } else {
            if (tirada_jugador2 < tirada_jugador1) {
                tirada_jugador1 = tirada_jugador2 - 2;
                tirada_jugador2 += 2;
            }
        }
        resultat.add(tirada_jugador1);
        resultat.add(tirada_jugador2);
    }
}

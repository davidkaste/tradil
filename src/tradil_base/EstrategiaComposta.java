/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

/**
 *
 * @author decline
 */
public abstract class EstrategiaComposta implements IEstrategia {

    protected List<IEstrategia> estrategies;

    /**
     * Constructor
     */
    public EstrategiaComposta() {
        estrategies = new ArrayList();
    }

    /**
     * Afegeix l'estratègia a la llista estratègies.
     * @param strat Estratègia
     */
    public void addEstrategia(IEstrategia strat) {
        estrategies.add(strat);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Observer;
import tradil_base.*;

/**
 *
 * @author decline
 */
public class Tradil {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        IFactoriaJugadors fact_jugadors = new FactoriaJugadorEstandar();
        System.out.println("== Simulació del dilema dels viatgers iterat ==");
        System.out.println("== 1. Nova partida");
        System.out.println("== 2. Sortir");
        String opcio = "";
        try {
            opcio = br.readLine();
        } catch (IOException ioe) {
            System.out.println("Error en llegir de teclat: " + ioe.getMessage());
        }

        switch (opcio) {
            case "1":
                ConfiguradorPartida p = new ConfiguradorPartida();
                IFactoriaRondes factoria_rondes = new FactoriaRondaConcreta();
                int numrondes = 0;
                try {
                    System.out.println("== Indica el número de rondes de la partida: ");
                    numrondes = Integer.parseInt(br.readLine());
                } catch (Exception e) {
                    System.out.println("Error en llegir de teclat: " + e.getMessage());
                }
                p.setRondes(factoria_rondes, numrondes);
                String jug1 = "";
                String jug2 = "";
                try {
                    System.out.println("== Indica el nom del primer jugador: ");
                    jug1 = br.readLine();
                    System.out.println("== Indica el nom del segon jugador: ");
                    jug2 = br.readLine();
                } catch (Exception e) {
                    System.out.println("Error en llegir de teclat: " + e.getMessage());
                }
                System.out.println("== Selecciona una estratègia per " + jug1);
                IEstrategia strat_jug1 = fabricaEstrategia(p);
                System.out.println("== Selecciona una estratègia per " + jug2);
                IEstrategia strat_jug2 = fabricaEstrategia(p);

                p.addJugador(fact_jugadors, strat_jug1, jug1);
                p.addJugador(fact_jugadors, strat_jug2, jug2);

                opcio = "1";
                while (!opcio.equals("3")) {
                    System.out.println("== Afegir observadors");
                    System.out.println("== 1. Observador estàndar");
                    System.out.println("== 2. Observador acumulat");
                    System.out.println("== 3. Executar partida");
                    try {
                        opcio = br.readLine();
                    } catch (IOException ioe) {
                        System.out.println("Error en llegir de teclat: " + ioe.getMessage());
                    }
                    switch (opcio){
                        case "1":
                            p.addObserver(new ObservadorEstandar());
                            break;
                        case "2":
                            p.addObserver(new ObservadorAcumulat());
                            break;
                    }
                }
                
                Partida part = p.build();
                part.executarPartida();

                break;
            case "2":
                System.out.println("== Fins una altra! ==");
                break;
            default:
                System.out.println("Opció no vàlida");
        }
    }

    public static IEstrategia fabricaEstrategia(ConfiguradorPartida p) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        IEstrategia strat = null;
        System.out.println("== 1. Estratègia aleatòria");
        System.out.println("== 2. Estratègia intel·ligent");
        System.out.println("== 3. Estratègia intel·ligent alternativa");
        System.out.println("== 4. Estratègia XOsa");
        System.out.println("== 5. Estratègia Ambiciosa");
        System.out.println("== 6. Estratègia Espavilada");
        System.out.println("== 7. Estratègia composta");
        String opcio = "";
        try {
            opcio = br.readLine();
        } catch (IOException ioe) {
            System.out.println("Error en llegir de teclat: " + ioe.getMessage());
        }
        switch (opcio) {
            case "1":
                strat = new EstrategiaAleatoria(100);
                break;
            case "2":
                strat = new EstrategiaIntelligent();
                p.addObserver((Observer)strat);
                break;
            case "3":
                strat = new EstrategiaIntelligentAlternativa();
                p.addObserver((Observer)strat);
                break;
            case "4":
                System.out.println("== Indica el número (2 - 100): ");
                int num = 0;
                try {
                    num = Integer.parseInt(br.readLine());
                } catch (Exception e) {
                    System.out.println("Error en llegir el número: " + e.getMessage());
                }
                strat = new EstrategiaXOsa(num);
                break;
            case "5":
                strat = new EstrategiaXOsa(100);
                break;
            case "6":
                strat = new EstrategiaXOsa(99);
                break;
            case "7":
                strat = fabricaEstrategiaComposta(p);
                break;
        }
        return strat;
    }

    public static IEstrategia fabricaEstrategiaComposta(ConfiguradorPartida p) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        EstrategiaComposta strat = null;
        System.out.println("== Tipus d'estratègia composta:");
        System.out.println("== 1. Valor mínim");
        System.out.println("== 2. Valor mitjà");
        String opcio = "";
        try {
            opcio = br.readLine();
        } catch (IOException ioe) {
            System.out.println("Error en llegir el número: " + ioe.getMessage());
        }

        switch (opcio) {
            case "1":
                strat = new EstrategiaCompostaMinim();
                break;
            case "2":
                strat = new EstrategiaCompostaMitjana();
                break;
        }

        opcio = "1";
        while (opcio.equals("1")) {
            System.out.println("== Fabricar estratègia composta:");
            System.out.println("== 1. Afegir estratègia simple");
            System.out.println("== 2. Sortir");
            try {
                opcio = br.readLine();
            } catch (IOException ioe) {
                System.out.println("Error en llegir el número: " + ioe.getMessage());
            }
            switch (opcio) {
                case "1":
                    System.out.println("== Selecciona una estratègia");
                    System.out.println("== 1. Estratègia aleatòria");
                    System.out.println("== 2. Estratègia intel·ligent");
                    System.out.println("== 3. Estratègia intel·ligent alternativa");
                    System.out.println("== 4. Estratègia XOsa");
                    System.out.println("== 5. Estratègia Ambiciosa");
                    System.out.println("== 6. Estratègia Espavilada");
                    String opcio2 = "";
                    try {
                        opcio2 = br.readLine();
                    } catch (IOException ioe) {
                        System.out.println("Error en llegir el número: " + ioe.getMessage());
                    }
                    switch (opcio2) {
                        case "1":
                            strat.addEstrategia(new EstrategiaAleatoria());
                            break;
                        case "2":
                            IEstrategia aux = new EstrategiaIntelligent();
                            strat.addEstrategia(aux);
                            p.addObserver((Observer)aux);
                            break;
                        case "3":
                            System.out.println("== Indica el número (2 - 100): ");
                            int num = 0;
                            try {
                                num = Integer.parseInt(br.readLine());
                            } catch (Exception e) {
                                System.out.println("Error en llegir el número: " + e.getMessage());
                            }
                            strat.addEstrategia(new EstrategiaXOsa(num));
                            break;
                        case "4":
                            strat.addEstrategia(new EstrategiaXOsa(100));
                            break;
                        case "5":
                            strat.addEstrategia(new EstrategiaXOsa(99));
                            break;
                    }
                    break;
                case "2":
                    return strat;
            }
        }
        return null;
    }
}

LLICÈNCIA
=========

![GitHub Logo](http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png)

Tradil de David Castellà està subjecta a una llicència de Reconeixement-NoComercial-CompartirIgual 3.0 No adaptada de Creative Commons
Creat a partir d'una obra disponible a github.com

DIAGRAMA
========

![GitHub Logo](http://img853.imageshack.us/img853/7549/travelerdilemmauml.png)

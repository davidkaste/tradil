/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author decline
 */
public class EstrategiaAleatoriaTest {

    IEstrategia strat = new EstrategiaAleatoria(100);

    public EstrategiaAleatoriaTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of calcularTirada method, of class EstrategiaAleatoria.
     */
    @Test
    public void testCalcularTirada() {
        System.out.println("calcularTirada");
        for (int i = 0; i < 50; i++) {
            strat = new EstrategiaAleatoria(i);
            int result = strat.calcularTirada();
            assertTrue(result >= 2 && result <= 100);
        }
    }
}

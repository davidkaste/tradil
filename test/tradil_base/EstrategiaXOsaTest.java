/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tradil_base;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author decline
 */
public class EstrategiaXOsaTest {
    
    IEstrategia strat = new EstrategiaXOsa(100);
    IEstrategia strat2 = new EstrategiaXOsa(99);
    IEstrategia strat3 = new EstrategiaXOsa(50);
    
    public EstrategiaXOsaTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcularTirada method, of class EstrategiaXOsa.
     */
    @Test
    public void testCalcularTirada() {
        System.out.println("calcularTirada");
        int expResult = 100;
        int result = strat.calcularTirada();
        assertEquals(expResult, result);
        
        expResult = 99;
        result = strat2.calcularTirada();
        assertEquals(expResult, result);
        
        expResult = 50;
        result = strat3.calcularTirada();
        assertEquals(expResult, result);
    }
}
